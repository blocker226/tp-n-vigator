# README #
Good day visitor! If you're a member of the public, welcome to this little prototype revolving around GPS navigation and AI pathfinding, made for a school hackathon, and still being made.

If you're a team member, do contribute code to help make this better! Every commit made will automatically be built using Unity Cloud Build. Let me know if you need a link to access your latest cloud build.

### What is this repository for? ###

* To be elaborated

### How do I get set up? ###

**For hackathon team members:**

* Get SourceTree + Bitbucket Account
* Clone this repo to somewhere convenient for you to access (preferably somewhere where you don't have to move the folder around)
* Install the latest stable version of Unity3D's editor (Current version 5.4.x)
* Start coding! Be sure to run through stuff with me before submitting pull requests.

### Contribution guidelines ###

* To be elaborated.

### Who do I talk to? ###
All enquiries can be submitted to the following email.

* Repo owner: neotwt@gmail.com (Blocker226)