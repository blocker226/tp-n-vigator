﻿using UnityEngine;
using System.Collections;

namespace UIStyles
{
	public class Outline : UnityEngine.UI.Outline 
	{
		/*
			This Outline class just inherits from unitys Outline, so attaching this class to your Text or Image will be the same as attaching unitys Outline, 
			Doing it this way you can create your own outline, use this class for your code or just inherit from your own outline class and then UI Styles can use your outline insted of unitys
			
			If you add your own outline use these variable name so UI Styles can controll the variables, if you need more variables and want help adding them contact me though the settings tab, ill be happy to help.

			public bool		useOutline;
			public Color	outlineColor	= new Color (0,0,0,.5f);
			public Vector2	outlineDistance = new Vector2(1, -1);
			public bool		outlineUseAlpha = true;


			Alternatively you could just use custom components in UI Styles 3.0
		*/
	}
}





















