﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using Vectrosity;

public class PathTracer : MonoBehaviour {

    public Camera vectorCam;
    public Color32 color;
	public float width = 1;
	NavMeshAgent agent;
    VectorLine line;
    bool lineDrawn = false;

	// Use this for initialization
	void Start () {
        VectorLine.SetCamera3D(vectorCam);
		agent = GameObject.FindGameObjectWithTag("Player").GetComponent<NavMeshAgent>();
	}

    public void MakePath (Vector3 startCoords, Vector3 destinationCoords) {
        DestroyPath();
        NavMeshPath newPath = new NavMeshPath();
        NavMesh.CalculatePath(startCoords, destinationCoords, agent.areaMask, newPath);
        if(newPath.status == NavMeshPathStatus.PathComplete) {
            TracePath(newPath);
            print("Blazing the trail!");
        }
    }
    
    public void DestroyPath() {
        if (lineDrawn) {
            VectorLine.Destroy(ref line);
            lineDrawn = false;
        }
    }

	void TracePath (NavMeshPath navpath) {
        //TODO: Add smoothing (Generate 4 points minimum for 3 segments)
        List<Vector3> pointList;
        pointList = navpath.corners.ToList();
        if (!lineDrawn) {
            line = new VectorLine("GPSPath", pointList, width);
            line.SetColor(color);
            line.SetWidth(width);
            line.Draw3D();
            lineDrawn = true;
        }
        else {
            line.points3 = pointList;
            line.Draw3D();
        }
    }
}
