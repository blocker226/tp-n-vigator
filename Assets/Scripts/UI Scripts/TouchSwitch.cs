﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchSwitch : MonoBehaviour {

    bool toggled = false;
    Animator slider;

    //A simple slider jerry-rigged to become a toggle switch.
    void Start () {
        slider = this.GetComponent<Animator>();
    } 

    public void ToggleSwitch () {
        toggled = !toggled;
        slider.SetBool("Switch", toggled ? true : false);
    }
	
}
