﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FlipWebApps.BeautifulTransitions.Scripts.Transitions;

public class ToggleUI : MonoBehaviour {

	//Add this component to whatever you want to toggle Beautiful Transitions on. Use a UI toggle to toggle on/off.
	
	public void ToggleTransition(bool value) {
        if (value) {
            TransitionHelper.TransitionIn(gameObject);
        }
        else if (!value) {
            TransitionHelper.TransitionOut(gameObject);
        }
    }
}
