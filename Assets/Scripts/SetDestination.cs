﻿using UnityEngine;

public class SetDestination : SelectableObject {

    [Sirenix.OdinInspector.AssetList(Path = "Destinations")]
    public GPSDestination destination;
    public Vector3 offset;

	public override void Focus() {
        base.Focus();

        Debug.Log("Selecting Destination " + destination.name);
        BTManager.instance.currentDestination = this;
        if (BTManager.instance.moveMarker) {
            GameObject.FindWithTag("Marker").transform.position = transform.position;
        }
        CreateWaypoint();
    }

    public override void Defocus() {
        Debug.Log("Unselecting Destination " + destination.name);
        BTManager.instance.currentDestination = null;
    }
    
    public GPSDestination GetDestination() {
        return destination;
    }

    public Vector3 GetPosition() {
        return transform.position;
    }

    void CreateWaypoint() {
        if (GameObject.FindWithTag("Waypoint")) {
            Destroy(GameObject.FindWithTag("Waypoint"));
        }
        Instantiate(BTManager.instance.waypointPrefab, transform.position + offset, Quaternion.identity);
    }
}
