﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////        EnviroSky- Renders a SkyDome with sun,moon,clouds and weather.          ////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;


[Serializable]
public class ObjectVariables // References - setup these in inspector! Or use the provided prefab.
{
	public GameObject Sun        = null;
	public GameObject Moon       = null;
	public GameObject Atmosphere = null;
	public GameObject Clouds = null;
}

[Serializable]
public class TimeVariables // GameTime variables
{
	[Header("Date and Time")]
	public bool ProgressTime = true;
    public float Hours  = 12f;  // 0 -  24 Hours day
    public float Days = 1f; //  1 - 365 Days of the year
	public float Years = 1f; // Count the Years maybe usefull!
	public float DayLengthInMinutes = 30; // DayLenght in realtime minutes
	[Range(0,24)]
	public float NightTimeInHours = 18f; 
	[Range(0,24)]
	public float MorningTimeInHours = 5f; 
	[Header("Location")]
    public float Latitude   = 0f;   // -90,  90   Horizontal earth lines
    public float Longitude  = 0f;   // -180, 180  Vertical earth line

}

[Serializable] 
public class LightVariables // All Lightning Variables
{
	[Header("Direct Light")]
    public float SunIntensity = 0.75f;
	public float MoonIntensity = 0.5f;
	public float MoonPhase = 0.0f;

	public Gradient DirectLightColor;
	public Gradient SunDiskColor;

	[Header("Ambient Light")]
	public Gradient AmbientLightColor;
	public AnimationCurve ambientLightIntenisty;

	[Header("Stars")]
	public AnimationCurve starsIntensity;
	[HideInInspector]public float SunWeatherMod = 0.0f;
}


[Serializable]
public class FogSettings 
{
	public FogMode Fogmode;
	public Gradient baseFogColor;
}

[Serializable]
public class CloudVariables // Default cloud settings, will be changed on runtime if Weather is enabled!
{
	[Header("Runtime Settings")]
	public Color BaseColor;
	public Color ShadingColor;
    public float Coverage = 1.0f; 
	public float Sharpness = 0.5f;
	public Vector2 WindDir = new Vector2 (1f, 1f);
	public AnimationCurve lightInfluence;

}


[ExecuteInEditMode]
[AddComponentMenu("Enviro/Sky System")]
public class EnviroSky : MonoBehaviour
{
    // Parameters
	public ObjectVariables Components = null;
	public TimeVariables GameTime  = null;
	public LightVariables  Lighting   = null;
	public CloudVariables Clouds = null;
	public FogSettings Fog = null;

    public Texture2D SkyGradientTexture = null;

	// Private Variables
	private int domeSeg = 32;
	private bool isNight = true;

    //Some Pointers
    private Transform DomeTransform;
	private Material  AtmosphereShader;
    private Material  FlatCloudShader;
    private Transform SunTransform;
	private Renderer  SunRenderer;
	private Material  SunShader;
	private Light     SunLight;
	private Transform MoonTransform;
	private Renderer  MoonRenderer;
	private Material  MoonShader;
	private Light     MoonLight;
	private EnviroMgr EnvMgr;

	private float lastHourUpdate;

	private float OrbitRadius
    {
        get { return DomeTransform.localScale.x; }
    }

    // PI
    const float pi = Mathf.PI;

    void Start()
    {
		lastHourUpdate = GameTime.Hours;
    }

    void OnEnable()
    {
        DomeTransform = transform;

		// Setup Fog
		RenderSettings.fogMode = Fog.Fogmode;
		//Check for needed Objects and define startup vars 
		if (Components.Atmosphere)
        {
			AtmosphereShader = Components.Atmosphere.GetComponent<Renderer>().sharedMaterial;

			MeshFilter filter = Components.Atmosphere.GetComponent<MeshFilter>();

            if (filter != null)
            {
                if (filter.sharedMesh != null)
                {
                    UnityEngine.Object.DestroyImmediate(filter.sharedMesh);
                }
				// Create the gradientDome mesh and assign it
                Mesh mesh = new Mesh();
				CreateDome(mesh, domeSeg);
                filter.sharedMesh = mesh;
            }
        }

		if (Components.Clouds)
        {
			FlatCloudShader = Components.Clouds.GetComponent<Renderer>().sharedMaterial;
        }
		else if (Components.Clouds == null)
		{
			Debug.LogError("Please set FastClouds object in inspector!");
			this.enabled = false;
		}

		if (Components.Sun)
        {
			SunTransform = Components.Sun.transform;
			SunRenderer = Components.Sun.GetComponent<Renderer>();
            SunShader = SunRenderer.sharedMaterial;
			SunLight = Components.Sun.GetComponent<Light>();
        }
        else
        {
            Debug.LogError("Please set Sun object in inspector!");
            this.enabled = false;
        }

		if (Components.Moon)
        {
			MoonTransform = Components.Moon.transform;
			MoonRenderer = Components.Moon.GetComponent<Renderer>();
            MoonShader = MoonRenderer.sharedMaterial;
			MoonLight = Components.Moon.GetComponent<Light>();
        }
        else
        {
			Debug.LogError("Please set Moon object in inspector!");
            this.enabled = false;
        }

		if(!EnviroMgr.instance.PlayerCamera)
		{
			Debug.LogError("Please assign your MainCamera in EnviroMgr-component!");
			this.enabled = false;
		}

		if(!EnviroMgr.instance.Audio.SpringDayAmbient || !EnviroMgr.instance.Audio.SummerDayAmbient || !EnviroMgr.instance.Audio.AutumnDayAmbient || !EnviroMgr.instance.Audio.WinterDayAmbient)
		{
			Debug.LogError("Please assign your day audioclip in EnviroSky-component!");
			this.enabled = false;
		}
		if(!EnviroMgr.instance.Audio.SpringNightAmbient || !EnviroMgr.instance.Audio.SummerNightAmbient || !EnviroMgr.instance.Audio.AutumnNightAmbient || !EnviroMgr.instance.Audio.WinterNightAmbient)
		{
			Debug.LogError("Please assign your night audioclip in EnviroSky-component!");
			this.enabled = false;
		}

		EnvMgr = GetComponent<EnviroMgr> ();
    }

	void UpdateQuality()
	{
		if (EnviroMgr.instance.Quality.RenderClouds)
			Components.Clouds.SetActive(true);
		else
			Components.Clouds.SetActive(false);
	}

	void UpdateAmbientLight ()
	{
		RenderSettings.ambientIntensity = Lighting.ambientLightIntenisty.Evaluate (GameTime.Hours / 24f);
		RenderSettings.ambientLight = Lighting.AmbientLightColor.Evaluate (GameTime.Hours / 24f);
	}

	void UpdateFogColor ()
	{
		RenderSettings.fogColor = Lighting.AmbientLightColor.Evaluate (GameTime.Hours / 24f);
	}

	Vector3 CalculatePosition ()
	{
		Vector3 newPosition;
		newPosition.x = EnviroMgr.instance.Player.transform.position.x;
		newPosition.z = EnviroMgr.instance.Player.transform.position.z;
		newPosition.y = EnviroMgr.instance.AdvancedPositioning.fixedSkyHeight;

		return newPosition;
	}
		
    void LateUpdate()
    {
        UpdateTime();
		ValidateParameters();
		UpdateQuality ();
		UpdateAmbientLight ();
		UpdateFogColor ();

        // Calculates the Solar latitude
        float latitudeRadians = Mathf.Deg2Rad * GameTime.Latitude;
        float latitudeRadiansSin = Mathf.Sin(latitudeRadians);
        float latitudeRadiansCos = Mathf.Cos(latitudeRadians);

		// Calculates the Solar longitude
		float longitudeRadians = Mathf.Deg2Rad * GameTime.Longitude;

        // Solar declination - constant for the whole globe at any given day
		float solarDeclination = 0.4093f * Mathf.Sin(2f * pi / 368f * (GameTime.Days - 81f));
        float solarDeclinationSin = Mathf.Sin(solarDeclination);
        float solarDeclinationCos = Mathf.Cos(solarDeclination);

        // Calculate Solar time
		float timeZone = (int)(GameTime.Longitude / 15f);
        float meridian = Mathf.Deg2Rad * 15f * timeZone;
		float solarTime = GameTime.Hours + 0.170f * Mathf.Sin(4f * pi / 373f * (GameTime.Days - 80f)) - 0.129f * Mathf.Sin(2f * pi / 355f * (GameTime.Days - 8f))  + 12f / pi * (meridian - longitudeRadians);
        float solarTimeRadians = pi / 12f * solarTime;
        float solarTimeSin = Mathf.Sin(solarTimeRadians);
        float solarTimeCos = Mathf.Cos(solarTimeRadians);

        // Solar altitude angle between the sun and the horizon
        float solarAltitudeSin = latitudeRadiansSin * solarDeclinationSin - latitudeRadiansCos * solarDeclinationCos * solarTimeCos;
        float solarAltitude = Mathf.Asin(solarAltitudeSin);

        // Solar azimuth angle of the sun around the horizon
        float solarAzimuthY = -solarDeclinationCos * solarTimeSin;
        float solarAzimuthX = latitudeRadiansCos * solarDeclinationSin - latitudeRadiansSin * solarDeclinationCos * solarTimeCos;
        float solarAzimuth = Mathf.Atan2(solarAzimuthY, solarAzimuthX);

        // Convert to spherical coords
        float coord = pi / 2 - solarAltitude;
        float phi = solarAzimuth;

        // Update sun position
        SunTransform.position = DomeTransform.position + DomeTransform.rotation * SphericalToCartesian(OrbitRadius, coord, phi);
        SunTransform.LookAt(DomeTransform.position);

        // Update moon position
        MoonTransform.position = DomeTransform.position + DomeTransform.rotation * SphericalToCartesian(OrbitRadius, coord + pi, phi);
        MoonTransform.LookAt(DomeTransform.position);

        // Update sun and fog color according to the new position of the sun
		SetupSunAndMoonColor(coord);
		SetupShader(coord);

		if (EnviroMgr.instance.PlayerCamera != null)
        {
			if (!EnviroMgr.instance.AdvancedPositioning.FixedHeight)
				transform.position = EnviroMgr.instance.PlayerCamera.transform.position;
			else 
			{
				transform.position = CalculatePosition ();
			}

			transform.localScale = new Vector3(EnviroMgr.instance.PlayerCamera.farClipPlane - (EnviroMgr.instance.PlayerCamera.farClipPlane * 0.1f), EnviroMgr.instance.PlayerCamera.farClipPlane - (EnviroMgr.instance.PlayerCamera.farClipPlane * 0.1f), EnviroMgr.instance.PlayerCamera.farClipPlane - (EnviroMgr.instance.PlayerCamera.farClipPlane * 0.1f));
        }

		if (!isNight && (GameTime.Hours >= GameTime.NightTimeInHours || GameTime.Hours <= GameTime.MorningTimeInHours) && EnviroMgr.instance.AudioSourceAmbient!=null)
		{
			switch (EnviroMgr.instance.seasons.currentSeasons)
			{
			case SeasonVariables.Seasons.Spring:
				EnviroMgr.instance.AudioSourceAmbient.clip = EnviroMgr.instance.Audio.SpringNightAmbient;
				break;
				
			case SeasonVariables.Seasons.Summer:
				EnviroMgr.instance.AudioSourceAmbient.clip = EnviroMgr.instance.Audio.SummerNightAmbient;
				break;
				
			case SeasonVariables.Seasons.Autumn:
				EnviroMgr.instance.AudioSourceAmbient.clip = EnviroMgr.instance.Audio.AutumnNightAmbient;
				break;
				
			case SeasonVariables.Seasons.Winter:
				EnviroMgr.instance.AudioSourceAmbient.clip = EnviroMgr.instance.Audio.WinterNightAmbient;
				break;
			}
			
			EnviroMgr.instance.AudioSourceAmbient.loop = true;
			EnviroMgr.instance.AudioSourceAmbient.Play();
			isNight = true;
			EnviroMgr.instance.NotifyIsNight ();

		}
		else if (isNight && (GameTime.Hours <= GameTime.NightTimeInHours && GameTime.Hours >= GameTime.MorningTimeInHours)&& EnviroMgr.instance.AudioSourceAmbient!=null)
		{
			
			switch (EnviroMgr.instance.seasons.currentSeasons)
			{
			case SeasonVariables.Seasons.Spring:
				EnviroMgr.instance.AudioSourceAmbient.clip = EnviroMgr.instance.Audio.SpringDayAmbient;
				break;
				
			case SeasonVariables.Seasons.Summer:
				EnviroMgr.instance.AudioSourceAmbient.clip = EnviroMgr.instance.Audio.SummerDayAmbient;
				break;
				
			case SeasonVariables.Seasons.Autumn:
				EnviroMgr.instance.AudioSourceAmbient.clip = EnviroMgr.instance.Audio.AutumnDayAmbient;
				break;
				
			case SeasonVariables.Seasons.Winter:
				EnviroMgr.instance.AudioSourceAmbient.clip = EnviroMgr.instance.Audio.WinterDayAmbient;
				break;
			}

			EnviroMgr.instance.AudioSourceAmbient.loop = true;
			EnviroMgr.instance.AudioSourceAmbient.Play();
			isNight = false;
			EnviroMgr.instance.NotifyIsDay ();
		}
	
    }

	// Setup the Shaders with correct information
    private void SetupShader(float setup)
    {
        Vector3 objSpaceSunDir = DomeTransform.InverseTransformDirection(SunTransform.forward);

        Color sunHaloColor = SunLight.color;
		sunHaloColor.a *= SunLight.intensity / (Lighting.SunIntensity + Lighting.SunWeatherMod);

        if (AtmosphereShader != null)
        {
            AtmosphereShader.SetColor("_SunColor", SunLight.color);
            AtmosphereShader.SetVector("_SunDirection", SunTransform.forward);
			AtmosphereShader.SetMatrix ("_Rotation", SunTransform.worldToLocalMatrix);
        }


		float altitude = pi/2 - setup;
		float altitude_abs = Mathf.Abs(altitude);

		if (FlatCloudShader != null && EnviroMgr.instance.Quality.RenderClouds)
        {
			FlatCloudShader.SetColor("_BaseColor", Clouds.BaseColor);
			FlatCloudShader.SetColor("_ShadingColor", Clouds.ShadingColor);
			FlatCloudShader.SetColor("_Ambient",Lighting.AmbientLightColor.Evaluate(GameTime.Hours / 24f));
			FlatCloudShader.SetFloat("_CloudCover", Clouds.Coverage);
			FlatCloudShader.SetFloat("_CloudSharpness", Clouds.Sharpness);
			FlatCloudShader.SetFloat("_timeV", Clouds.lightInfluence.Evaluate(GameTime.Hours / 24f));
			FlatCloudShader.SetVector("_CloudSpeed", Clouds.WindDir);
        }

        if (MoonShader != null)
        {
			MoonShader.SetFloat("_Phase", Lighting.MoonPhase);
        }

        if (SunShader != null)
        {
			SunShader.SetColor("_Color", Lighting.SunDiskColor.Evaluate(GameTime.Hours / 24f));
        }
    }

	// Update the GameTime
    void UpdateTime()
    {
		float oneDay = GameTime.DayLengthInMinutes * 60f;
        float oneHour = oneDay / 24f;

        float hourTime = Time.deltaTime / oneHour;
        float moonTime = Time.deltaTime / (30f * oneDay) * 2f;

        if (GameTime.ProgressTime) // Calculate Time
        {
			GameTime.Hours += hourTime;
			Lighting.MoonPhase += moonTime;

			if (Lighting.MoonPhase < -1) Lighting.MoonPhase += 2;
			else if (Lighting.MoonPhase > 1) Lighting.MoonPhase -= 2;

			if (GameTime.Hours >= lastHourUpdate + 1f) 
			{
				lastHourUpdate = GameTime.Hours;
				EnviroMgr.instance.NotifyHourPassed ();
			}

			if (GameTime.Hours >= 24)
            {
				GameTime.Hours = 0;
				EnviroMgr.instance.NotifyHourPassed ();
				lastHourUpdate = 0f;
				GameTime.Days = GameTime.Days + 1;
				EnviroMgr.instance.NotifyDayPassed ();
            }

			if(GameTime.Days >= (EnvMgr.seasons.SpringInDays + EnvMgr.seasons.SummerInDays + EnvMgr.seasons.AutumnInDays + EnvMgr.seasons.WinterInDays))
			{
				GameTime.Years = GameTime.Years + 1;
				GameTime.Days = 0;
				EnviroMgr.instance.NotifyYearPassed ();
			}

        }
    }

    // Calculate sun and moon color
    private void SetupSunAndMoonColor(float setup)
    {
		SunLight.color = Lighting.DirectLightColor.Evaluate (GameTime.Hours / 24f);
		MoonLight.color = Lighting.DirectLightColor.Evaluate (GameTime.Hours / 24f);

        // Sun altitude and intensity dropoff angle
        float altitude = pi/2 - setup;
        float altitude_abs = Mathf.Abs(altitude);
        float dropoff_rad = 10 * Mathf.Deg2Rad;
		

        // Set sun and moon intensity
        if (altitude > 0)
        {
			SunLight.enabled = true;
			MoonLight.enabled = false;
			float sunIntensityMax = Lighting.SunIntensity + Lighting.SunWeatherMod;
           
			if (altitude_abs < dropoff_rad)
            {
                SunLight.intensity = Mathf.Lerp(0, sunIntensityMax, altitude_abs / dropoff_rad);
            }
			else SunLight.intensity = Mathf.Lerp(SunLight.intensity, sunIntensityMax,0.01f);

        }
        else
        {
			SunLight.enabled = false;
			MoonLight.enabled = true;

			float moonIntensityMax = Lighting.MoonIntensity * Mathf.Clamp01(1 - Mathf.Abs(Lighting.MoonPhase));
           
			if (altitude_abs < dropoff_rad)
            {
                MoonLight.intensity = Mathf.Lerp(0, moonIntensityMax, altitude_abs / dropoff_rad);
            }
            else 
				MoonLight.intensity = moonIntensityMax;

        }
    }

    // Make the parameters stay in reasonable range
    private void ValidateParameters()
    {
        // Keep GameTime Parameters right!
		GameTime.Hours = Mathf.Repeat(GameTime.Hours, 24);
		//GameTime.Days = Mathf.Repeat(GameTime.Days - 1, (EnvMgr.seasons.SpringInDays + EnvMgr.seasons.SummerInDays + EnvMgr.seasons.AutumnInDays + EnvMgr.seasons.WinterInDays)) + 1;
		GameTime.Longitude = Mathf.Clamp(GameTime.Longitude, -180, 180);
		GameTime.Latitude = Mathf.Clamp(GameTime.Latitude, -90, 90);

		#if UNITY_EDITOR
		if (GameTime.DayLengthInMinutes == 0)
		{
			GameTime.Hours = 12f;
			Lighting.MoonPhase = 0f;
		}
		#endif

		// Give correct preview in EditoMode:
        // Sun
        #if UNITY_EDITOR
		Lighting.SunIntensity = Mathf.Max(0, Lighting.SunIntensity);
        #endif

        // Moon
        #if UNITY_EDITOR
		Lighting.MoonIntensity = Mathf.Max(0, Lighting.MoonIntensity);
		Lighting.MoonPhase = Mathf.Clamp(Lighting.MoonPhase, -1, +1);
        #endif

        // Clouds
        #if UNITY_EDITOR
        Clouds.Coverage = Mathf.Max(-1, Clouds.Coverage);
        #endif
    }

    // Convert spherical coordinates to cartesian coordinates
    private Vector3 SphericalToCartesian(float radius, float theta, float phi)
    {
        Vector3 res;

        float sinTheta = Mathf.Sin(theta);
        float cosTheta = Mathf.Cos(theta);
        float sinPhi   = Mathf.Sin(phi);
        float cosPhi   = Mathf.Cos(phi);

        res.x = radius * sinTheta * cosPhi;
        res.y = radius * cosTheta;
        res.z = radius * sinTheta * sinPhi;

        return res;
    }

    //Create the skyDome mesh
    void CreateDome (Mesh mesh, int segments)
    {
        Vector3[] vertices = new Vector3[segments * (segments - 1) + 2];
        Vector3[] normals = new Vector3[segments * (segments - 1) + 2];
        Vector2[] uv = new Vector2[segments * (segments - 1) + 2];

        int[] indices = new int[2 * segments * (segments - 1) * 3];

        float deltaLatitude = pi / (float)segments;
        float deltaLongitude = pi * 2.0f / (float )segments;

        // Generate the rings
        int index = 0;
        for (int i = 1; i < segments; i++) 
		{
            float r0 = Mathf.Sin (i * deltaLatitude);
            float y0 = Mathf.Cos (i * deltaLatitude);

            for (int j = 0; j < segments; j++) 
			{
                float x0 = r0 * Mathf.Sin (j * deltaLongitude);
                float z0 = r0 * Mathf.Cos (j * deltaLongitude);

                vertices[index].x = x0;
                vertices[index].y = y0;
                vertices[index].z = z0;

                normals[index].x = -x0;
                normals[index].y = -y0;
                normals[index].z = -z0;

                uv[index].x = 0;
                uv[index].y = 1 - y0;

                index++;
            }
        }

        // Generate the UPside
        vertices[index].x = 0;
        vertices[index].y = 1;
        vertices[index].z = 0;

        normals[index].x = 0;
        normals[index].y = -1;
        normals[index].z = 0;

        uv[index].x = 0;
        uv[index].y = 0;

        index++;

        vertices[index].x = 0;
        vertices[index].y = -1;
        vertices[index].z = 0;

        normals[index].x = 0;
        normals[index].y = 1;
        normals[index].z = 0;

        uv[index].x = 0;
        uv[index].y = 2;

        index = 0;
        // Generate the midSide
        for (int i = 0; i < segments - 2; i++) 
		{
            for (int j = 0; j < segments; j++) 
			{
                indices[index++] = segments * i + j;
                indices[index++] = segments * i + (j + 1) % segments;
                indices[index++] = segments * (i + 1) + (j + 1) % segments;
                indices[index++] = segments * i + j;
                indices[index++] = segments * (i + 1) + (j + 1) % segments;
                indices[index++] = segments * (i + 1) + j;
            }
        }

        // Generate the upper cap
        for (int i = 0; i < segments; i++) 
		{
            indices[index++] = segments * (segments - 1);
            indices[index++] = (i + 1) % segments;
            indices[index++] = i;
        }

        // Generate the lower cap
        for (int i = 0; i < segments; i++)
		{
            indices[index++] = segments * (segments - 1) + 1;
            indices[index++] = segments * (segments - 2) + i;
            indices[index++] = segments * (segments - 2) + (i + 1) % segments;
        }

        mesh.vertices = vertices;
        mesh.normals = normals;
        mesh.uv = uv;
        mesh.triangles = indices;
    }
}
