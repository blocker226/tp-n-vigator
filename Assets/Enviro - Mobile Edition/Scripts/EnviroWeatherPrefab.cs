﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class EnviroWeatherPrefab : MonoBehaviour {

	public string Name;
	[Header("Season Settings")]
	public bool Spring = true;
	[Range(1,100)]
	public float possibiltyInSpring = 100f;
	public bool Summer = true;
	[Range(1,100)]
	public float possibiltyInSummer = 100f;
	public bool Autumn = true;
	[Range(1,100)]
	public float possibiltyInAutumn = 100f;
	public bool winter = true;
	[Range(1,100)]
	public float possibiltyInWinter = 100f;
	

    [HideInInspector] public List<float> effectEmmisionRates = new List<float>();

	[Header("Cloud Settings")]
	public EnviroWeatherCloudConfig cloudConfig;

	[Header("Fog Settings")]
	public float linearStartDistance;
	public float linearEndDistance;
	public float expDensity;

	[Header("Weather Settings")]
	public List<ParticleSystem> effectParticleSystems = new List<ParticleSystem>();

	public float sunLightMod = 0.0f;
	[Range(0,1)]
	public float WindStrenght = 0.5f;
	[Range(0,1)]
	public float wetnessLevel = 0f;
	[Range(0,1)]
	public float snowLevel = 0f;
	public bool isLightningStorm;

	[Header("Audio Settings")]
	public AudioClip Sfx;
}

